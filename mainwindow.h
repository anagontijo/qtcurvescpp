#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_spin_scale_valueChanged(double arg1);

    void on_spin_length_valueChanged(double arg1);

    void on_spin_step_count_valueChanged(int arg1);

    void on_btn_spiral_clicked();

    void on_btn_bg_color_clicked();

    void on_btn_line_color_clicked();

    void on_shape_option_activated(int index);

    void enable_abc();

    void enable(int i);

    void on_spin_a_valueChanged(double arg1);

    void on_spin_b_valueChanged(double arg1);

    void on_spin_c_valueChanged(double arg1);

private:
    Ui::MainWindow *ui;

    void update_ui();
};
#endif // MAINWINDOW_H
