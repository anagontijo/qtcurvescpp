#include "renderarea.h"
#include <QPaintEvent>
#include <QPainter>
#include <math.h>

RenderArea::RenderArea(QWidget *parent) : QWidget(parent),
    my_background_color(0,0,255),
    my_shape_color(255,255,255),
    my_shape(Astroid)
{
    on_shape_changed();
}

QSize RenderArea::minimumSizeHint() const {
    return QSize(100, 100);
}
QSize RenderArea::sizeHint() const {
    return QSize(400, 200);
}

QPointF RenderArea::compute(float t){
    switch(my_shape){
        case Astroid:
            return compute_astroid(t);
        case Cycloid:
            return compute_cycloid(t);
        case HuygensCycloid:
            return compute_huygens_cycloid(t);
        case HypoCycloid:
            return compute_hypo_cycloid(t);
        case Line:
            return compute_line(t);
        case Spiral:
            return compute_spiral(t);
        case Circle:
            return compute_circle(t);
        case Ellipse:
            return compute_ellipse(t);
        case Fancy:
            return compute_fancy(t);
        case StarFish:
            return compute_starfish(t);
        case Parabol:
            return compute_parabol(t);
        default:
            return QPointF(0,0);
    }
}

QPointF RenderArea::compute_astroid(float t){
    float cos_t = cos(t);
    float sin_t = sin(t);
    float x = 2 * cos_t * cos_t * cos_t;
    float y = 2 * sin_t * sin_t * sin_t;
    return QPointF(x,y);
}

QPointF RenderArea::compute_cycloid(float t){
    return QPointF(
                1.5 * (1 - cos(t)),
                1.5 * (t - sin(t))
                );
}

QPointF RenderArea::compute_huygens_cycloid(float t){
    return QPointF(
                4 * (3 * cos(t) - cos(3 * t)),
                4 * (3 * sin(t) - sin(3 * t))
                );
}

QPointF RenderArea::compute_hypo_cycloid(float t){
    return QPointF(
                1.5 * (2 * cos(t) + cos(2 * t)),
                1.5 * (2 * sin(t) - sin(2 * t))
                );
}

QPointF RenderArea::compute_line(float t){
    return QPointF(
                1 - t,
                1 - t
                );
}

QPointF RenderArea::compute_spiral(float t){
    return QPointF(
                t * cos(t),
                t * sin(t)
                );
}

QPointF RenderArea::compute_circle(float t){
    return QPointF(
                cos(t),
                sin(t)
                );
}

QPointF RenderArea::compute_ellipse(float t){
    return QPointF(
                my_a * cos(t),
                my_b * sin(t)
                );
}

QPointF RenderArea::compute_fancy(float t){
    return QPointF(
                my_a * cos(t) - my_b * cos((my_a/my_b) * t),
                my_a * sin(t) - my_b * sin((my_a/my_b) * t)
                );
}

QPointF RenderArea:: compute_starfish(float t){
    return QPointF(
                (my_a - my_b) * cos(t) + my_c * cos(t * ((my_a - my_b)/my_b)),
                (my_a - my_b) * sin(t) - my_c * sin(t * ((my_a - my_b)/my_b))
                );
}

QPointF RenderArea::compute_parabol(float t){
    float x = t - my_length/2;
    return QPointF(
                x,
                my_a*x*x + my_b*x + my_c
                );
}

void RenderArea::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // Drawing area
    painter.setBrush(my_background_color);
    painter.setPen(my_shape_color);

    painter.drawRect(this->rect());

    QPoint center = this->rect().center();
    float step = my_length / my_step_count;
    QPointF prev_pixel_f = compute(0);
    QPoint prev_pixel(
                prev_pixel_f.x()*my_scale + center.x(),
                prev_pixel_f.y()*my_scale + center.y());
    for (float t=0; t < my_length; t += step ){
        QPointF point = compute(t);
        QPoint pixel;
        pixel.setX(point.x()*my_scale + center.x());
        pixel.setY(point.y()*my_scale + center.y());

        painter.drawLine(pixel, prev_pixel);
        prev_pixel = pixel;
    }
}

void RenderArea::on_shape_changed(){
    switch(my_shape){
        case Astroid:
            my_scale = 90;
            my_length = 2 * M_PI;
            my_step_count = 256;
            break;
        case Cycloid:
            my_scale = 10;
            my_length = 4 * M_PI;
            my_step_count = 128;
            break;
        case HuygensCycloid:
            my_scale = 4;
            my_length = 4 * M_PI;
            my_step_count = 256;
            break;
        case HypoCycloid:
            my_scale = 40;
            my_length = 2 * M_PI;
            my_step_count = 256;
            break;
        case Line:
            my_scale = 100;
            my_length = 2;
            my_step_count = 128;
            break;
        case Spiral:
            my_scale = 3;
            my_length = 99;
            my_step_count = 512;
            break;
        case Circle:
            my_scale = 100;
            my_length = 2 * M_PI;
            my_step_count = 128;
            break;
        case Ellipse:
            my_scale = 75;
            my_length = 2 * M_PI;
            my_step_count = 256;
            my_a = 2;
            my_b = 1.1;
            break;
        case Fancy:
            my_scale = 10;
            my_length = 12 * M_PI;
            my_step_count = 512;
            my_a = 11;
            my_b = 6;
            break;
        case StarFish:
            my_scale = 25;
            my_length = 6 * M_PI;
            my_step_count = 256;
            my_a = 5;
            my_b = 3;
            my_c = 5;
            break;
        case Parabol:
            my_scale = 25;
            my_length = 100;
            my_step_count = 256;
            my_a = 1;
            my_b = 0;
            my_c = 0;
            break;
        default:
            break;
    }
}

std::vector<int> RenderArea::uses_abc(){
    switch(my_shape){
        case Ellipse:
            return std::vector<int>{1,1,0};
        case Fancy:
            return std::vector<int>{1,1,0};
        case StarFish:
            return std::vector<int>{1,1,1};
        case Parabol:
            return std::vector<int>{1,1,1};
        default:
            return std::vector<int>{0,0,0};
    }
}
