#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QColor>

class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = nullptr);

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;

    enum Shapes {Astroid, Cycloid, HuygensCycloid, HypoCycloid, Line,
                 Spiral, Circle, Ellipse, Fancy,
                 StarFish, Parabol};

    void setBackgroundColor(QColor color){my_background_color = color; repaint();}
    QColor getBackgroundColor() const {return my_background_color;}

    void setShapeColor(QColor color){my_shape_color = color; repaint();}
    QColor getShapeColor() const {return my_shape_color;}

    void setShape(Shapes shape){my_shape = shape; on_shape_changed();}
    Shapes getShape(){return my_shape;}

    void setScale(float scale){my_scale = scale; repaint();}
    float getScale() const {return my_scale;}

    void setLength(float length){my_length = length; repaint();}
    float getLength(){return my_length;}

    void setStepCount(int step_count){my_step_count = step_count; repaint();}
    int getStepCount(){return my_step_count;}

    void setA(float a){my_a = a; repaint();}
    float getA(){return my_a;}

    void setB(float b){my_b = b; repaint();}
    float getB(){return my_b;}

    void setC(float c){my_c = c; repaint();}
    float getC(){return my_c;}

    std::vector<int> uses_abc();
protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
private:
    QColor my_background_color;
    QColor my_shape_color;
    Shapes my_shape;

    float my_length;
    float my_scale;
    int my_step_count;

    float my_a;
    float my_b;
    float my_c;

    QPointF compute(float t);

    QPointF compute_astroid(float t);
    QPointF compute_cycloid(float t);
    QPointF compute_huygens_cycloid(float t);
    QPointF compute_hypo_cycloid(float t);
    QPointF compute_line(float t);
    QPointF compute_spiral(float t);
    QPointF compute_circle(float t);
    QPointF compute_ellipse(float t);
    QPointF compute_fancy(float t);
    QPointF compute_starfish(float t);
    QPointF compute_parabol(float t);
    void on_shape_changed();
signals:

};

#endif // RENDERAREA_H
