#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QColorDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    enable_abc();
    update_ui();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::enable_abc(){

    enum abc_enum {a, b, c};

    std::vector<int> abc = this->ui->render_area->uses_abc();
    for(int i = 0; i < 3; i++){
        switch(abc_enum(i)){
            case a:
                this->ui->spin_a->setVisible(bool(abc[i]));
                this->ui->a_label->setVisible(bool(abc[i]));
                this->ui->spin_a->setValue(this->ui->render_area->getA());
                break;
            case b:
                this->ui->spin_b->setVisible(bool(abc[i]));
                this->ui->b_label->setVisible(bool(abc[i]));
                this->ui->spin_b->setValue(this->ui->render_area->getB());
                break;
            case c:
                this->ui->spin_c->setVisible(bool(abc[i]));
                this->ui->c_label->setVisible(bool(abc[i]));
                this->ui->spin_c->setValue(this->ui->render_area->getC());
                break;
            default:
                break;
        }
    }
}


void MainWindow::on_shape_option_activated(int shape)
{
    this->ui->render_area->setShape(RenderArea::Shapes(shape));
    this->ui->render_area->repaint();
    enable_abc();
    update_ui();

}

void MainWindow::update_ui(){
    this->ui->spin_scale->setValue(this->ui->render_area->getScale());
    this->ui->spin_length->setValue(this->ui->render_area->getLength());
    this->ui->spin_step_count->setValue(this->ui->render_area->getStepCount());
}

void MainWindow::on_spin_scale_valueChanged(double scale)
{
    this->ui->render_area->setScale(scale);
}

void MainWindow::on_spin_length_valueChanged(double length)
{
    this->ui->render_area->setLength(length);
}

void MainWindow::on_spin_step_count_valueChanged(int step_count)
{
    this->ui->render_area->setStepCount(step_count);
}

void MainWindow::on_spin_a_valueChanged(double a)
{
    this->ui->render_area->setA(a);
}

void MainWindow::on_spin_b_valueChanged(double b)
{
    this->ui->render_area->setB(b);
}

void MainWindow::on_spin_c_valueChanged(double c)
{
    this->ui->render_area->setC(c);
}

void MainWindow::on_btn_bg_color_clicked()
{
    QColor color = QColorDialog::getColor(this->ui->render_area->getBackgroundColor(), this, "Select background color");
    this->ui->render_area->setBackgroundColor(color);
}

void MainWindow::on_btn_line_color_clicked()
{
    QColor color = QColorDialog::getColor(this->ui->render_area->getShapeColor(), this, "Select line color");
    this->ui->render_area->setShapeColor(color);
}
